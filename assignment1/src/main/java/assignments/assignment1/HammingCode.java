package assignments.assignment1;

import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;
    
    /**
     * Method untuk menjumlahkan setiap angka pada string.
     */
    public static int sumIntegerChar(String s) {
        int sum = 0;
        for (int i = 0; i < s.length(); i++) {
            sum += Character.getNumericValue(s.charAt(i));
        }
        return sum;
    }
    
    /**
     * Untuk mengecek tiap pola pada hammingCode agar apabila dijumlahkan harus genap.
     * Hanya untuk method encode()
     */
    public static String hammingCodeValidator(String newString, int r) {
        StringBuilder str = new StringBuilder(newString); //membuat objek string builder
        //agar bisa mengganti item pada string
        for (int i = 0; i < r; i++) { //iterasi sebanyak bit redundant
            int x = (int)Math.pow(2, i);  
            String strNew = ""; // string baru untuk dimasukkan bit yang akan dicek
            for (int j = x; j < newString.length() + 1; j += Math.pow(2, i + 1)) {
                //untuk mengecek apakah index substring lebih panjang dari panjang string
                if (j + x < newString.length() + 1) {                 
                    strNew += newString.substring(j - 1, j - 1 + x); 
                } else {
                    strNew += newString.substring(j - 1); 
                }
            }
            if (sumIntegerChar(strNew) % 2 != 0) { 
                str.setCharAt(x - 1, '1'); 
            }
        }
        return str.toString();
    }
    
    /**
     *Untuk mengubah suatu data menjadi hammingCode.
     */
    public static String encode(String data) {
        int m = data.length();
        int r = 1;
        while (Math.pow(2, r) < m + r + 1) {
            r++;
        } // mencari r
        
        int length = r + m; //panjang string hamming code
        int j = 0;
        String newString = "";
        for (int i = 1; i < length + 1; i++) {
            // mengecek apakah index ke-i merupakan 2 pangkat n
            if ((Math.ceil(Math.log(i) / Math.log(2)) 
                - Math.floor(Math.log(i) / Math.log(2))) == 0) { 
                newString += "0"; //inisiasi bit redundant dengan angka 0
            } else {
                newString += data.charAt(j); //memasukkan tiap bit data ke newString
                j++; //apabila index bukan 2 pangkat n
            }
        }
        return hammingCodeValidator(newString, r); //mengecek tiap pola hammingCode
    }
    
    /**
     *Untuk mengartikan suatu hamming code menjadi data. 
     *Otomatis memvalidasi code yang diterima
     */
    public static String decode(String code) {
        int r = 1;
        while (Math.pow(2, r) < code.length() + 1) {
            r++;
        } //mencari r
        
        StringBuilder str = new StringBuilder(code);
        //membuat objek string builder agar bisa mengganti item pada string

        int errorIndex = 0; //inisiasi index error pada hamming code dengan 0
        for (int i = 0; i < r; i++) {
            int x = (int)Math.pow(2, i);
            String strNew = "";
            for (int j = x; j < code.length() + 1; j += Math.pow(2, i + 1)) {
                if (j + x < code.length() + 1) {
                    strNew += code.substring(j - 1, j - 1 + x);
                } else {
                    strNew += code.substring(j - 1, code.length());
                }
            }
            //menjumlahkan item tiap pola dan cek apakah genap
            if (sumIntegerChar(strNew) % 2 != 0) {
                errorIndex += x; //apabila tidak error index bertambah x
            }
        }
        //jika error index > 0 yang menandakan ada kesalahan pada code
        if (errorIndex > 0) {
            if (code.charAt(errorIndex - 1) == '1') {
                str.setCharAt(errorIndex - 1, '0');
            //mengganti bit pada index error dengan bit yang sesuai
            } else {
                str.setCharAt(errorIndex - 1, '1');
            }
        }
        
        code = str.toString();
        String data = "";
        
        // Menambahkan setiap data dengan index bukan 2 pangkat n ke data
        for (int i = 1; i < code.length() + 1; i++) {
            if ((Math.ceil(Math.log(i) / Math.log(2))
                    - Math.floor(Math.log(i) / Math.log(2))) != 0) {
                data += code.charAt(i - 1);
            }
        }
        return data;
    }
    

    /**
     * Main program for Hamming Code.
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
