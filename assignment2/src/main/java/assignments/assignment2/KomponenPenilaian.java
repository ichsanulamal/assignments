package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
    	this.nama = nama;
    	this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
    	this.bobot = bobot;
    	
        // buat constructor untuk KomponenPenilaian.
        // banyakButirPenilaian digunakan untuk menentukan panjang butirPenilaian saja
        // (tanpa membuat objek-objek ButirPenilaian-nya).
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
    	this.butirPenilaian[idx] = butir;
        // masukkan butir ke butirPenilaian pada index ke-idx.
    }

    public String getNama() {
        // kembalikan nama KomponenPenilaian.
        return this.nama;
    }

    public double getRerata() {
    	// kembalikan rata-rata butirPenilaian.
    	double mean = 0.0;
    	int banyakKomponenTerisi = 0;
    	for (int i = 0; i < butirPenilaian.length; i++) {
    		if (butirPenilaian[i] != null) {
    			mean += butirPenilaian[i].getNilai();
    			banyakKomponenTerisi ++;
    		}
    	}
    	
    	mean /= banyakKomponenTerisi;
    	if (mean >= 0) {
    		return mean;
    	}
        return 0.00;
    }

    public double getNilai() {
        // kembalikan rerata yang sudah dikalikan dengan bobot.
        return this.getRerata() * this.bobot / 100;
    }

    public String getDetail() {
    	String strAkhir = "~~~ "+ this.nama + " (" + this.bobot + "%) ~~~\n";
    	for(int i = 0; i < butirPenilaian.length; i++) {
    			strAkhir += this.nama + " " + (i + 1) + ": ";
    			if(butirPenilaian[i] == null) {
    				strAkhir += "kosong (belum diinput)\n";
    			} else {
    				strAkhir += butirPenilaian[i].toString() +"\n";
    			}
    	}
    	if (butirPenilaian.length > 1) {
    		strAkhir += this.toString() + "\n";
    	}

		strAkhir += "Kontribusi nilai akhir: " + String.format("%.2f", this.getNilai())+ "\n";
        // kembalikan detail KomponenPenilaian sesuai permintaan soal.
        return strAkhir;
    }

    @Override
    public String toString() {
        // kembalikan representasi String sebuah KomponenPenilaian sesuai permintaan soal.
        return "Rerata " + this.nama + ": " + String.format("%.2f",this.getRerata());
    }  

}
