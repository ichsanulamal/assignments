package assignments.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AsistenDosen {
    private List<Mahasiswa> lstMahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
    	this.kode = kode;
    	this.nama = nama;
        // buat constructor untuk AsistenDosen.
    }

    public String getKode() {
        // kembalikan kode AsistenDosen.
        return this.kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
    	this.lstMahasiswa.add(mahasiswa);
    	Collections.sort(this.lstMahasiswa);
        // tambahkan mahasiswa ke dalam daftar mahasiswa dengan mempertahankan urutan.
        // kamu boleh menggunakan Collections.sort atau melakukan sorting manual.
        // manfaatkan method compareTo pada Mahasiswa.
    }

    public Mahasiswa getMahasiswa(String npm) {
    	for (int i=0; i < this.lstMahasiswa.size(); i++) {
    		if (this.lstMahasiswa.get(i).getNpm().equals(npm)) {
    			return this.lstMahasiswa.get(i);
    		}
    	}
        // kembalikan objek Mahasiswa dengan NPM tertentu dari daftar mahasiswa.
        // jika tidak ada, kembalikan null atau lempar sebuah Exception.
        return null;
    }

    public String rekap() {
    	String strAkhir = "";
    	for(int i = 0; i < this.lstMahasiswa.size(); i++) {
    		strAkhir += this.lstMahasiswa.get(i).toString() + "\n" 
    				+ this.lstMahasiswa.get(i).rekap() + "\n";
    	}
        return strAkhir;
    }
    
    public void ubahMahasiswa(Mahasiswa mahasiswa, AsistenDosen other) {
    	this.lstMahasiswa.remove(mahasiswa);
    	other.addMahasiswa(mahasiswa);
    }

    public String toString() {
        return this.kode + " - " + this.nama;
    }
}
