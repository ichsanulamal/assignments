package assignments.assignment2;

import java.util.Scanner;

public class GlasDOS {
    private static final int INPUT_NUM = 1;
    private static final int DETAIL_NUM = 2;
    private static final int REKAP_NUM = 3;
    private static final int UBAH_KODE_NUM = 4;
    private static final int KELUAR_NUM = 99;
    private AsistenDosen[] asistenDosen;
    private KomponenPenilaian[] templatSkemaPenilaian;
    private Scanner input = new Scanner(System.in);

    private void initPesan() {
        System.out.println("Selamat datang di Global Asdos Data Organizing System!");
        System.out.println("====================== GlasDOS =======================");
        System.out.println();
    }

    // Method untuk mengisi skema penilaian.
    private void initSkemaPenilaian() {
        System.out.println("Mari buat skema penilaian!");
        System.out.println("Masukkan banyaknya komponen penilaian:");
        int numKomponen = Integer.parseInt(input.nextLine());
        templatSkemaPenilaian = new KomponenPenilaian[numKomponen];
        for (int i = 1; i <= numKomponen; i++) {
            System.out.println("> Komponen Penilaian " + i);
            System.out.println("Masukkan nama komponen penilaian " + i + ": ");
            String namaKomponen = input.nextLine();
            System.out.println("Masukkan banyaknya butir penilaian " + namaKomponen + ":");
            int numButir = Integer.parseInt(input.nextLine());
            System.out.println("Masukkan bobot penilaian " + namaKomponen + " (dalam persen):");
            int bobot = Integer.parseInt(input.nextLine());
            // Memasukkan objek KomponenPenilaian ke array templatSkemaPenilaian
            templatSkemaPenilaian[i - 1] = new KomponenPenilaian(namaKomponen, numButir, bobot);
        }
        System.out.println("Skema penilaian berhasil dibuat!");
        System.out.println();
    }

    // Method untuk mengisi jumlah dan rincian asdos.
    private void initAsdosMahasiswa() {
        System.out.println("Mari masukkan data asdos dan mahasiswa!");
        System.out.println("Masukkan banyaknya asdos:");
        int numAsistenDosen = Integer.parseInt(input.nextLine());
        // Menginisiasi array asistenDosen sejumlah numAsistenDosen
        asistenDosen = new AsistenDosen[numAsistenDosen];
        
        // Iterasi sebanyak jumlah Asisten Dosen 
        for (int i = 1; i <= numAsistenDosen; i++) {
            System.out.println("Data asdos " + i + ":");
            String[] dataAsdos = input.nextLine().split(" ", 2);
            // Membuat objek asdos
            AsistenDosen newAsdos = new AsistenDosen(dataAsdos[0], dataAsdos[1]);
            asistenDosen[i - 1] = newAsdos; // Memasukkan objek asdos ke array
            System.out.println("Masukkan banyaknya mahasiswa dengan kode asdos " + dataAsdos[0] + ":");
            int numAsdosan = Integer.parseInt(input.nextLine());
            
            // Iterasi sebanyak jumlah mahasiswa di bawah asdos tsb
            for (int j = 1; j <= numAsdosan; j++) {
                System.out.println("Data mahasiswa " + j + ":");
                String[] dataMhs = input.nextLine().split(" ", 2);
                // Menyalin KomponenPenilaian yg sudah dibuat untuk dimasukkan ke objek mahasiswa
                KomponenPenilaian[] skema = KomponenPenilaian.salinTemplat(templatSkemaPenilaian);
                Mahasiswa baru = new Mahasiswa(dataMhs[0], dataMhs[1], skema);
                newAsdos.addMahasiswa(baru); // Menambah mahasiswa ke asdos terkait
            }
        }
        System.out.println();
    }

    // Method untuk mencari objek asisten dosen dengan memasukkan kodenya.
    private AsistenDosen getAsistenDosen(String kode) {
    	for (int i = 0; i < asistenDosen.length; i ++) {
    		if (asistenDosen[i].getKode().equalsIgnoreCase(kode)) {
    			return asistenDosen[i];
    		}
    	}
        return null;
    }

    // Method untuk kembalikan Mahasiswa dengan NPM dan kodeAsdos tertentu.
    private Mahasiswa getMahasiswa(String kodeAsdos, String npm) {
    	if (getAsistenDosen(kodeAsdos) == null) {
    		return null;
    	} else if (getAsistenDosen(kodeAsdos).getMahasiswa(npm) == null) {
    		return null;
    	}
    	return getAsistenDosen(kodeAsdos).getMahasiswa(npm);
    }

    // kembalikan KomponenPenilaian dengan namaKomponen tertentu
    // dari seorang Mahasiswa dengan NPM tertentu dan kodeAsdos tertentu.
    private KomponenPenilaian getKomponenPenilaian(String kodeAsdos, String npm, String namaKomponen) {
    	if (getMahasiswa(kodeAsdos, npm) == null) {
    		return null;
    	} else if (getMahasiswa(kodeAsdos, npm).getKomponenPenilaian(namaKomponen) == null) {
    		return null;
    	}
    	return getMahasiswa(kodeAsdos, npm).getKomponenPenilaian(namaKomponen);
    }
    
    // Method untuk menjalankan perintah input nilai tiap mahasiswa
    private void menuInput() {
        System.out.println("--- Input nilai ---");
        System.out.println("Masukkan perintah input nilai dalam format berikut:");
        System.out.println(
            "[KodeAsdos] [NPM] [KomponenPenilaian] [NomorButirPenilaian] [Nilai] [Terlambat]"
        );
        System.out.println("Contoh: SMA 1234567890 Lab 1 90.00 true");
        System.out.println("Jika sudah selesai, tulis SELESAI dan tekan tombol Enter.");
        System.out.println();
        while (!input.hasNext("(?i)SELESAI")) {
            String masukan[] = input.nextLine().split(" ");
            String kodeAsdos = masukan[0];
            String npm = masukan[1];
            String namaKomponen = masukan[2];
            int idx = Integer.parseInt(masukan[3]);
            double nilaiButir = Double.parseDouble(masukan[4]);
            boolean terlambat = Boolean.parseBoolean(masukan[5]);
            // Membuat objek ButirPenilaian dan memasukkannya ke komponenPenilaian
            ButirPenilaian butir = new ButirPenilaian(nilaiButir, terlambat);
            KomponenPenilaian komponenPenilaian = getKomponenPenilaian(kodeAsdos, npm, namaKomponen);
            
            if (komponenPenilaian != null) {
                komponenPenilaian.masukkanButirPenilaian(idx - 1, butir);
            } else {
                System.out.println(
                    "Komponen penilaian untuk NPM " + npm + " pada kode asdos "
                    + kodeAsdos + "tidak ditemukan!"
                );
            };
        }
        input.nextLine();
        System.out.println("Sukses! Kembali ke menu utama...");
        System.out.println();
    }

    // Mencari detail penilaian dari suatu mahasiswa.
    private void menuDetail() {
        System.out.println("--- Lihat detail nilai mahasiswa ---");
        System.out.println("Masukkan data mahasiswa dalam format berikut:");
        System.out.println("[KodeAsdos] [NPM]");
        System.out.println("Contoh: SMA 134567890");
        System.out.println();
        String[] dataMhs = input.nextLine().split(" ");
        Mahasiswa mahasiswa = getMahasiswa(dataMhs[0], dataMhs[1]);
        if (mahasiswa != null) {
            System.out.println(mahasiswa);
            System.out.println(mahasiswa.getDetail()); //salha
        } else {
            System.out.println("Mahasiswa dengan kode asdos dan NPM tersebut tidak ditemukan!");
        };
        System.out.println();
        System.out.println("Kembali ke menu utama...");
        System.out.println();
    }

    // Merekap semua asdos dan mahasiswa yang ditanggungjawabi.
    private void menuRekap() {
        System.out.println("--- Rekap ---");
        for (AsistenDosen asdos : asistenDosen) {
            System.out.println("###" + asdos + "###");
            System.out.println();
            System.out.println(asdos.rekap());
            System.out.println();
        }
        System.out.println("Kembali ke menu utama...");
        System.out.println();
    }
    
    // Mengubah kode asdos mahasiswa.
    private void menuUbah() {
    	System.out.println("--- Ubah kode Asdos mahasiswa ---");
        System.out.println("Masukkan NPM mahasiswa, kode asdos asal dan kode asdos baru\n"
        		+ "dengan format di bawah ini");
        System.out.println("[KodeAsdosAsal] [NPM] [KodeAsdosBaru]");
        System.out.println("Contoh: SMA 1234567890 SMP");
        System.out.println("Mahasiswa di bawah tanggung jawab KodeAsdosAwal\n"
        		+ "akan berpindah ke KodeAsdosBaru");
        System.out.println();
        
        String[] ArrayMhsAsdos = input.nextLine().split(" ");
        String asdosAwal = ArrayMhsAsdos[0];
        String mahasiswa = ArrayMhsAsdos[1];
        String asdosBaru = ArrayMhsAsdos[2];
        
        // Cek null tiap asdos dan mahasiswa
        if (getMahasiswa(asdosAwal, mahasiswa) == null) {
        	System.out.println("Mahasiswa tidak ditemukan!\n");
        } else if (getAsistenDosen(asdosAwal) == null) {
        	System.out.println("Asdos dengan kode "
        			+ asdosAwal + " tidak ditemukan!\n");
        } else if (getAsistenDosen(asdosBaru) == null) {
        	System.out.println("Asdos dengan kode "
        			+ asdosBaru + " tidak ditemukan!\n");
        } else if (getAsistenDosen(asdosAwal) == null &
        		getAsistenDosen(asdosBaru) == null) {
        	System.out.println("Asdos dengan kode "
        			+ asdosAwal + " dan " + asdosBaru + " tidak ditemukan!\n");
        } else {
        	// Operasi pengubahan kode asdos suatu mahasiswa
        	Mahasiswa mhs = getAsistenDosen(asdosAwal).getMahasiswa(mahasiswa);
        	AsistenDosen oAsdosAwal = getAsistenDosen(asdosAwal);
        	AsistenDosen oAsdosBaru = getAsistenDosen(asdosBaru);
        	oAsdosAwal.ubahMahasiswa(mhs, oAsdosBaru);
        	System.out.println();
        	System.out.println("Mahasiswa dengan NPM "
        			+ mhs.getNpm() + " telah berpindah dari kode asdos " 
        			+ asdosAwal + " menjadi " + asdosBaru + "\n");
        }  
    }

    private void menu() {
        int operation = 0;
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println("=== Menu utama ===");
            System.out.println("1. Input nilai");
            System.out.println("2. Lihat detail nilai mahasiswa");
            System.out.println("3. Rekap");
            System.out.println("4. Ubah kode asdos mahasiswa");
            System.out.println("99. Keluar");
            System.out.println("Masukkan pilihan menu:");
            try {
                operation = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (operation == INPUT_NUM) {
                menuInput();
            } else if (operation == DETAIL_NUM) {
                menuDetail();
            } else if (operation == REKAP_NUM) {
                menuRekap();
            } else if (operation == UBAH_KODE_NUM ) {
            	menuUbah();
            } else if (operation == KELUAR_NUM) {
                System.out.println("Terima kasih telah menggunakan GlasDOS!");
                hasChosenExit = true;
            }
        }
    }

    private void run() {
        initPesan();
        initSkemaPenilaian();
        initAsdosMahasiswa();
        System.out.println("Inisialisasi selesai!");
        System.out.println();
        menu();
        input.close();
    }

    // START...
    public static void main(String[] args) {
        GlasDOS program = new GlasDOS();
        program.run();
    }
}
