package assignments.assignment3;

public class SalahTipeException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Throw exception ini apabila membuat objek dengan tipe diluar ketentuan
     */
    public SalahTipeException(String message) {
        super(message);
    }
}
