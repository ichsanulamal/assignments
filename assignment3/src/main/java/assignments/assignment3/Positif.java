package assignments.assignment3;

public class Positif implements Status{
  
    public String getStatus(){
        return "Positif";
    }
    
    public void tularkan(Carrier penular, Carrier tertular){
        if (penular instanceof Benda && tertular instanceof Benda) {
            return;
        } else {
            if (tertular instanceof Manusia) {
                // Untuk penular
                penular.tambahKasusDisebabkan();

                // Untuk tertular
                tertular.ubahStatus("Positif");
                tertular.tambahRantai(penular);
            }
            else if(tertular instanceof Benda) {
                Benda benda = (Benda)tertular;
                benda.tambahPersentase();
                
                if (benda.getPersentaseMenular() >= 100) {
                    // Untuk penular
                    penular.tambahKasusDisebabkan();

                    // Untuk benda atau tertular
                    benda.ubahStatus("Positif");
                    benda.tambahRantai(penular);
                }
            }
        }
    }
        // Implementasikan apabila object Penular melakukan interaksi dengan object tertular
        // Hint: Handle kasus ketika keduanya benda dapat dilakukan disini
    
}