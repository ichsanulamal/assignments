package assignments.assignment3;

public class AngkutanUmum extends Benda{
      
    /**
     * Contructor
     */
    public AngkutanUmum(String name){
        super(name, "Angkutan Umum");
    }

    @Override
    /**
     * Menambah persentase penularan sebanyak 35
     */
    public void tambahPersentase() {
        super.persentaseMenular += 35;
    }

    @Override
    public String toString() {
        return "ANGKUTAN UMUM " + super.getNama();
    }
}