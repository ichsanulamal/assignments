package assignments.assignment3;

public abstract class Manusia extends Carrier{
    private static int jumlahSembuh = 0;
    
    /**
     * Contructor
     */
    public Manusia(String nama, String tipe){
        super(nama, tipe);
    }
    
    // Menambahkan nilai pada atribut jumlahSembuh
    public void tambahSembuh(){
        jumlahSembuh ++;
    }

    // Kembalikan nilai untuk atribut jumlahSembuh.
    public static int getJumlahSembuh() {
        return jumlahSembuh;
    }
}