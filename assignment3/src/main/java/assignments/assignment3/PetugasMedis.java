package assignments.assignment3;

public class PetugasMedis extends Manusia{
  		
    private int jumlahDisembuhkan;

    /**
     * Contructor
     */
    public PetugasMedis(String nama){
        super(nama, "Petugas Medis");
    }

    // PetugasMedis ini menyembuhkan manusia
    public void obati(Manusia manusia) { 
        manusia.ubahStatus("Negatif");
        manusia.resetRantaiPenular();
        jumlahDisembuhkan++;

        manusia.tambahSembuh(); // method pada Manusia
        // Update nilai atribut jumlahDisembuhkan
    }

    // Kembalikan nilai dari atribut jumlahDisembuhkan
    public int getJumlahDisembuhkan(){
        return jumlahDisembuhkan;
    }

    @Override
    public String toString() {
        return "PETUGAS MEDIS " + super.getNama();
    }
}