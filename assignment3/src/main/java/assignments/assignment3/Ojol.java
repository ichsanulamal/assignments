package assignments.assignment3;

public class Ojol extends Manusia{
      
    /**
     * Contructor
     */
    public Ojol(String name){
        super(name, "Ojol");
    }

    @Override
    public String toString() {
        return "OJOL " + super.getNama();
    }
  	
}