package assignments.assignment3;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InputOutput{
    private PrintWriter pw;
    private List<String> inputText; // Menggantikan bufferedReader
    private String inputFile;
    private String outputFile; 
    private World world;
     
    /**
     * Constructor
     */
    public InputOutput(String inputType, String inputFile, String outputType, String outputFile)
            throws FileNotFoundException {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        setBufferedReader(inputType);
        setPrintWriter(outputType);
    }

    /**
     * Menampung semua input dari text atau terminal ke List inputText
     */
    public void setBufferedReader(String inputType){
        inputText = new ArrayList<>();
        
        // Input dari file
        if (inputType.equalsIgnoreCase("text")) {
            try {
                // input seluruh teks dengan Kelas method dari kelas Files
                inputText = Files.readAllLines(Paths.get(inputFile));
            } 
            catch(Exception ex) {
                System.out.println(String.format("File dengan nama %s tidak ada",
                    inputFile));
            }
        
        // Input dari terminal
        } else {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Masukkan teks yang akan diinput:");
            System.out.println("Ketik \"EXIT\" apabila sudah selesai!");
            System.out.println();

            String text = "";
            while(!text.equalsIgnoreCase("exit")) {
                text = scanner.nextLine();
                inputText.add(text);
            }
            scanner.close();
        }
    }
    
    /**
     * Menampung file output ke pw apabila memilih output text
     * Objek pw menjadi null apabila memilih output terminal
     */
    public void setPrintWriter(String outputType) throws FileNotFoundException {
        if (outputType.equalsIgnoreCase("text")) {
            try {
            File file = new File(this.outputFile);
            pw = new PrintWriter(file);
            }
            catch(IOException ex) {
                System.out.println(String.format("File dengan nama %s tidak ada",
                    outputFile));
                pw = null;
            }
            
        }
    }

    /**
     * Program utama
     */
    public void run() throws IOException, BelumTertularException {
        world = new World();
        String output = "";

        
        // Menjalankan program dengan membaca semua isi inputText
        for(String text:inputText) {
            String[] perintah = text.split(" ");

            try {
                String query = perintah[0];
    
                /* QUERY ADD */
                if (query.equalsIgnoreCase("ADD")) {
                    String tipe = perintah[1];
                    String nama = perintah[2];
                    try {
                        world.createObject(tipe, nama); // membuat objek
                    } catch (SalahTipeException ex) {
                        System.out.println(String.format("Tidak ada tipe %s", tipe));
                    }
                    
                /* QUERY INTERAKSI */
                } else if(query.equalsIgnoreCase("INTERAKSI")) {
                    String objek1 = perintah[1];
                    String objek2 = perintah[2];
                    if (world.getCarrier(objek1) != null) {
                        if (world.getCarrier(objek2) != null) {
                            world.getCarrier(objek1).interaksi(world.getCarrier(objek2));
                        }
                    }

                /* QUERY POSITIFKAN */
                } else if(query.equalsIgnoreCase("POSITIFKAN")) {
                    // world.getCarrier(perintah[1]) != null &&
                    if(world.getCarrier(perintah[1]) instanceof Manusia ) {
                        world.getCarrier(perintah[1]).ubahStatus("Positif");
                    }

                /* QUERY SEMBUHKAN */
                } else if(query.equalsIgnoreCase("SEMBUHKAN")) {
                    if (world.getCarrier(perintah[1]) instanceof PetugasMedis &&
                            world.getCarrier(perintah[2]) instanceof Manusia) {
                        PetugasMedis petugas = (PetugasMedis)world.getCarrier(perintah[1]);
                        Manusia manusia = (Manusia)world.getCarrier(perintah[2]);
                        petugas.obati(manusia);   
                    }

                /* QUERY BERSIHKAN */
                } else if(query.equalsIgnoreCase("BERSIHKAN")) {
                    if (world.getCarrier(perintah[1]) instanceof CleaningService &&
                            world.getCarrier(perintah[2]) instanceof Benda) {
                        CleaningService petugas = (CleaningService)world.getCarrier(perintah[1]);
                        Benda benda  = (Benda)world.getCarrier(perintah[2]);
                        petugas.bersihkan(benda);    
                    }

                /* QUERY RANTAI */
                } else if(query.equalsIgnoreCase("RANTAI")) {
                    Carrier nama = world.getCarrier(perintah[1]);
                    try {
                        Carrier.cekStatus(nama);
                        output += "Rantai penyebaran " + nama + ": ";
                        List<Carrier> list = nama.getRantaiPenular();
                        for(Carrier carrier:list) {
                            output += carrier  + " -> ";
                        }
                        output += nama +"\n";
                    }
                    catch(BelumTertularException ex) {
                        output += ex.toString() + "\n";
                    }

                /* QUERY TOTAL_KASUS_DARI_OBJEK */
                } else if(query.equalsIgnoreCase("TOTAL_KASUS_DARI_OBJEK")) {
                    Carrier nama = world.getCarrier(perintah[1]);
                    int jumlahKasus = nama.getTotalKasusDisebabkan();
                    output += nama + " telah menyebarkan virus COVID ke "+
                    jumlahKasus +" objek\n";
                    
                /* QUERY AKTIF_KASUS_DARI_OBJEK */
                } else if(query.equalsIgnoreCase("AKTIF_KASUS_DARI_OBJEK")) {
                    Carrier nama = world.getCarrier(perintah[1]);
                    int jumlahKasusAktif = world.getCarrier(perintah[1]).getAktifKasusDisebabkan();
                    output += nama + " telah menyebarkan virus COVID dan masih " + 
                    "teridentifikasi positif sebanyak " + jumlahKasusAktif +" objek\n";
                    
                /* QUERY TOTAL_SEMBUH_MANUSIA */
                } else if(query.equalsIgnoreCase("TOTAL_SEMBUH_MANUSIA")) {
                    int jumlahManusiaSembuh = Manusia.getJumlahSembuh();
                    output += String.format("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: %s kasus\n",
                            jumlahManusiaSembuh);

                /* QUERY TOTAL_BERSIH_CLEANING_SERVICE */
                } else if(query.equalsIgnoreCase("TOTAL_SEMBUH_PETUGAS_MEDIS")) {
                    if (world.getCarrier(perintah[1]) instanceof PetugasMedis) {
                        PetugasMedis petugas = (PetugasMedis)(world.getCarrier(perintah[1]));
                        output += String.format("%s menyembuhkan %d manusia", petugas, petugas.getJumlahDisembuhkan()) + "\n";
                    }

                /* QUERY TOTAL_BERSIH_CLEANING_SERVICE */
                } else if(query.equalsIgnoreCase("TOTAL_BERSIH_CLEANING_SERVICE")) {
                    if (world.getCarrier(perintah[1]) instanceof CleaningService) {
                        CleaningService petugas = (CleaningService)(world.getCarrier(perintah[1]));
                        output += String.format("%s membersihkan %d benda", petugas, petugas.getJumlahDibersihkan()) + "\n";
                    }

                /* QUERY EXIT */
                } else if(query.equalsIgnoreCase("EXIT")) {
                    text = "";
                    break;
                
                } else {
                    System.out.println(String.format("Perintah \"%s\" tidak diketahui sistem", 
                        text));
                }
            }
            catch (Exception ex) {
                System.out.println(String.format("Perintah \"%s\" tidak diketahui sistem", 
                        text));
            }
        } 
        // Output file
        if(pw == null) {
            System.out.println(output);
        } else {
            String[] listOutput = output.split("\n");
            for (String line:listOutput) {
                pw.println(line);
            }
            pw.close();
            System.out.println(String.format("Sukses menyimpan ke file %s", outputFile));
        }
        // Program utama untuk InputOutput, jangan lupa handle untuk IOException
        // Hint: Buatlah objek dengan class World
        // Hint: Untuk membuat object Carrier baru dapat gunakan method CreateObject pada class World
        // Hint: Untuk mengambil object Carrier dengan nama yang sesua dapat gunakan method getCarrier pada class World
    }
    
    /*
     * Method khsus buat JavaFX
     * method ini return string output
     */
    public String runJFX() throws IOException, BelumTertularException {
        world = new World();
        String output = "";

        
        // Menjalankan program dengan membaca semua isi inputText
        for(String text:inputText) {
            String[] perintah = text.split(" ");

            try {
                String query = perintah[0];
    
                /* QUERY ADD */
                if (query.equalsIgnoreCase("ADD")) {
                    String tipe = perintah[1];
                    String nama = perintah[2];
                    try {
                        world.createObject(tipe, nama); // membuat objek
                    } catch (SalahTipeException ex) {
                        System.out.println(String.format("Tidak ada tipe %s", tipe));
                    }
                    
                /* QUERY INTERAKSI */
                } else if(query.equalsIgnoreCase("INTERAKSI")) {
                    String objek1 = perintah[1];
                    String objek2 = perintah[2];
                    if (world.getCarrier(objek1) != null) {
                        if (world.getCarrier(objek2) != null) {
                            world.getCarrier(objek1).interaksi(world.getCarrier(objek2));
                        }
                    }

                /* QUERY POSITIFKAN */
                } else if(query.equalsIgnoreCase("POSITIFKAN")) {
                    // world.getCarrier(perintah[1]) != null &&
                    if(world.getCarrier(perintah[1]) instanceof Manusia ) {
                        world.getCarrier(perintah[1]).ubahStatus("Positif");
                    }

                /* QUERY SEMBUHKAN */
                } else if(query.equalsIgnoreCase("SEMBUHKAN")) {
                    if (world.getCarrier(perintah[1]) instanceof PetugasMedis &&
                            world.getCarrier(perintah[2]) instanceof Manusia) {
                        PetugasMedis petugas = (PetugasMedis)world.getCarrier(perintah[1]);
                        Manusia manusia = (Manusia)world.getCarrier(perintah[2]);
                        petugas.obati(manusia);   
                    }

                /* QUERY BERSIHKAN */
                } else if(query.equalsIgnoreCase("BERSIHKAN")) {
                    if (world.getCarrier(perintah[1]) instanceof CleaningService &&
                            world.getCarrier(perintah[2]) instanceof Benda) {
                        CleaningService petugas = (CleaningService)world.getCarrier(perintah[1]);
                        Benda benda  = (Benda)world.getCarrier(perintah[2]);
                        petugas.bersihkan(benda);    
                    }

                /* QUERY RANTAI */
                } else if(query.equalsIgnoreCase("RANTAI")) {
                    Carrier nama = world.getCarrier(perintah[1]);
                    try {
                        Carrier.cekStatus(nama);
                        output += "Rantai penyebaran " + nama + ": ";
                        List<Carrier> list = nama.getRantaiPenular();
                        for(Carrier carrier:list) {
                            output += carrier  + " -> ";
                        }
                        output += nama +"\n";
                    }
                    catch(BelumTertularException ex) {
                        output += ex.toString() + "\n";
                    }

                /* QUERY TOTAL_KASUS_DARI_OBJEK */
                } else if(query.equalsIgnoreCase("TOTAL_KASUS_DARI_OBJEK")) {
                    Carrier nama = world.getCarrier(perintah[1]);
                    int jumlahKasus = nama.getTotalKasusDisebabkan();
                    output += nama + " telah menyebarkan virus COVID ke "+
                    jumlahKasus +" objek\n";
                    
                /* QUERY AKTIF_KASUS_DARI_OBJEK */
                } else if(query.equalsIgnoreCase("AKTIF_KASUS_DARI_OBJEK")) {
                    Carrier nama = world.getCarrier(perintah[1]);
                    int jumlahKasusAktif = nama.getAktifKasusDisebabkan();
                    output += nama + " telah menyebarkan virus COVID dan masih " + 
                    "teridentifikasi positif sebanyak " + jumlahKasusAktif +" objek\n";
                    
                /* QUERY TOTAL_SEMBUH_MANUSIA */
                } else if(query.equalsIgnoreCase("TOTAL_SEMBUH_MANUSIA")) {
                    int jumlahManusiaSembuh = Manusia.getJumlahSembuh();
                    output += String.format("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: %s kasus\n",
                            jumlahManusiaSembuh);

                /* QUERY TOTAL_TOTAL_SEMBUH_PETUGAS_MEDIS */
                } else if(query.equalsIgnoreCase("TOTAL_SEMBUH_PETUGAS_MEDIS")) {
                    if (world.getCarrier(perintah[1]) instanceof PetugasMedis) {
                        PetugasMedis petugas = (PetugasMedis)(world.getCarrier(perintah[1]));
                        output += String.format("%s menyembuhkan %d manusia", petugas, petugas.getJumlahDisembuhkan()) + "\n";
                    }

                /* QUERY TOTAL_BERSIH_CLEANING_SERVICE */
                } else if(query.equalsIgnoreCase("TOTAL_BERSIH_CLEANING_SERVICE")) {
                    if (world.getCarrier(perintah[1]) instanceof CleaningService) {
                        CleaningService petugas = (CleaningService)(world.getCarrier(perintah[1]));
                        output += String.format("%s membersihkan %d benda", petugas, petugas.getJumlahDibersihkan()) + "\n";
                    }

                /* QUERY EXIT */
                } else if(query.equalsIgnoreCase("EXIT")) {
                    text = "";
                    break;
                
                } else {
                    System.out.println(String.format("Perintah \"%s\" tidak diketahui sistem", 
                        text));
                }
            }
            catch (Exception ex) {
                System.out.println(String.format("Perintah \"%s\" tidak diketahui sistem", 
                        text));
            }
        } 
        return output;
        // Program utama untuk InputOutput, jangan lupa handle untuk IOException
        // Hint: Buatlah objek dengan class World
        // Hint: Untuk membuat object Carrier baru dapat gunakan method CreateObject pada class World
        // Hint: Untuk mengambil object Carrier dengan nama yang sesua dapat gunakan method getCarrier pada class World
    }
    
}