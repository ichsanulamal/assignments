package assignments.assignment3;

public class PeganganTangga extends Benda{
    
  	/**
     * Constructor
     */
    public PeganganTangga(String name){
        super(name, "Pegangan Tangga");
    }

    @Override
    /**
     * Menambah persentase penularan sebanyak 20
     */
    public void tambahPersentase() {
        super.persentaseMenular += 20;

    }

    @Override
    public String toString() {
        return "PEGANGAN TANGGA " + super.getNama();
    }
    
}