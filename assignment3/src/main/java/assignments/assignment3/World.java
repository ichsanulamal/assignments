package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;

    /**
     * Contructor
     */
    public World(){
        listCarrier = new ArrayList<>();
    }

    /**
     * Membuat objek carrier dengan parameter tipe, nama
     * 
     * @throws SalahTipeException
     */
    public Carrier createObject(String tipe, String nama) throws SalahTipeException {
        Carrier carrier = null;
        if(getCarrier(nama) == null) {
            if (tipe.equalsIgnoreCase("PEKERJA_JASA")) {
                carrier = new PekerjaJasa(nama);
            } else if (tipe.equalsIgnoreCase("CLEANING_SERVICE")) {
                carrier = new CleaningService(nama);
            } else if (tipe.equalsIgnoreCase("JURNALIS")) {
                carrier = new Jurnalis(nama);
            } else if (tipe.equalsIgnoreCase("OJOL")) {
                carrier = new Ojol(nama);
            } else if (tipe.equalsIgnoreCase("PETUGAS_MEDIS")) {
                carrier = new PetugasMedis(nama);
            } else if (tipe.equalsIgnoreCase("TOMBOL_LIFT")) {
                carrier = new TombolLift(nama);
            } else if (tipe.equalsIgnoreCase("PEGANGAN_TANGGA")) {
                carrier = new PeganganTangga(nama);
            } else if (tipe.equalsIgnoreCase("PINTU")) {
                carrier = new Pintu(nama);
            } else if (tipe.equalsIgnoreCase("ANGKUTAN_UMUM")) {
                carrier = new AngkutanUmum(nama);
            } else {
                throw new SalahTipeException("Tipe yang dimasukkan salah");
            }
            listCarrier.add(carrier);
        }         
        return carrier;
    }

    /**
     * Mencari Objek Carrier dengan nama tertentu pada World ini
     */
    public Carrier getCarrier(String nama){
        for(Carrier objek:listCarrier) {
            if (objek.getNama().equalsIgnoreCase(nama)) {
                return objek;
            }
        }
        return null;
    }
    
    public ArrayList<String> getListNama() {
    	ArrayList<String> list = new ArrayList<>();
    	for(Carrier carrier:listCarrier) {    		
    		list.add(carrier.getNama());
    	}
    	return list;
    }
    
    public ArrayList<String> getListManusia() {
    	ArrayList<String> list = new ArrayList<>();
    	for(Carrier carrier:listCarrier) {
    		if (carrier instanceof Manusia) {
    			list.add(carrier.getNama());
    		}
    	}
    	return list;
    }
    
    public ArrayList<String> getListPkebersihan() {
    	ArrayList<String> list = new ArrayList<>();
    	for(Carrier carrier:listCarrier) {
    		if (carrier instanceof CleaningService) {
    			list.add(carrier.getNama());
    		}
    	}
    	return list;
    }
    
    public ArrayList<String> getListMedis() {
    	ArrayList<String> list = new ArrayList<>();
    	for(Carrier carrier:listCarrier) {
    		if (carrier instanceof PetugasMedis) {
    			list.add(carrier.getNama());
    		}
    	}
    	return list;
    }
    
    public ArrayList<String> getListBenda() {
    	ArrayList<String> list = new ArrayList<>();
    	for(Carrier carrier:listCarrier) {
    		if (carrier instanceof Benda) {
    			list.add(carrier.getNama());
    		}
    	}
    	return list;
    }
}
