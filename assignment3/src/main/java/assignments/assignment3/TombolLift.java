package assignments.assignment3;

public class TombolLift extends Benda{
    
    /**
     * Constructor
     */
    public TombolLift(String name){
        super(name, "Tombol Lift");
    }

    /**
     * Menambah persentase penularan sebanyak 20
     */
    @Override
    public void tambahPersentase() {
        super.persentaseMenular += 20;
        // Auto-generated method stub
    }

    @Override
    public String toString() {
        return "TOMBOL LIFT " + super.getNama();
    }
}