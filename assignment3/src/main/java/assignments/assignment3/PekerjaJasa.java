package assignments.assignment3;

public class PekerjaJasa extends Manusia{
      
    /**
     * Contructor
     */
    public PekerjaJasa(String nama){
        super(nama, "Pekerja Jasa");
    }

    @Override
    public String toString() {
        return "PEKERJA JASA " + super.getNama();
    }
  	
}