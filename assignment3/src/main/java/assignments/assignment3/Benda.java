package assignments.assignment3;


public abstract class Benda extends Carrier{
    // Atribut
    protected int persentaseMenular;

    /**
     * Contructor
     */
    public Benda(String name, String tipe){
        super(name, tipe);
    }

    public abstract void tambahPersentase();

    public int getPersentaseMenular(){
        return persentaseMenular;
    }

    public void setPersentaseMenular(int persentase) {
        this.persentaseMenular = persentase;
    }
}